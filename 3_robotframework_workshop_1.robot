*Settings*
Library  BuiltIn


**Variables**

${cirArea}=  0
${r}=  4
${pi}=  3.14

**Test Cases**
1. คำนวนพื้นที่สี่เหลี่ยม
    ${cirArea}=  SqureArea  ${r}
    Set Global Variable  ${cirArea}

2. แสดงผล
    LOG to Console  \nArea: ${cirArea}

**Keywords**
SqureArea
    [Arguments]  ${r}
    ${area}=  Evaluate  ${r} * ${r} * ${pi}
    [Return]  ${area}